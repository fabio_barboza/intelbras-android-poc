package br.com.intelbras.poc.service

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.company.NetSDK.*

import java.io.File


class SdkService private constructor() {
    private var mbInit = false

    private val mDisconnect: DeviceDisConnect
    private val mReconnect: DeviceReConnect

    private val mHandler = Handler(Looper.myLooper())

    init {
        mDisconnect = DeviceDisConnect()
        mReconnect = DeviceReConnect()
    }

    @Synchronized
    fun init() {
        INetSDK.LoadLibrarys()
        if (mbInit) {
            Log.d(TAG, "Already init.")
            return
        }
        mbInit = true

        val zRet = INetSDK.Init(mDisconnect)
        if (!zRet) {
            Log.e(TAG, "init NetSDK error!")
            return
        }

        val stNetParam = NET_PARAM()
        stNetParam.nWaittime = TIMEOUT_10S
        stNetParam.nSearchRecordTime = TIMEOUT_30S
        INetSDK.SetNetworkParam(stNetParam)
    }

    @Synchronized
    fun cleanup() {
        if (mbInit) {
            INetSDK.Cleanup()
            mbInit = false
        }
    }

    inner class DeviceDisConnect : CB_fDisConnect {
        override fun invoke(loginHandle: Long, deviceIp: String, devicePort: Int) {
            Log.d(TAG, "Device $deviceIp is disConnected !")
            mHandler.post {
                Toast.makeText(null, "Device $deviceIp is disConnected !", Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class DeviceReConnect : CB_fHaveReConnect {
        override fun invoke(loginHandle: Long, deviceIp: String, devicePort: Int) {
            Log.d(TAG, "Device $deviceIp is reconnect !")
        }
    }

    companion object {
        private val TAG = "SdkService"
        val instance = SdkService()

        val TIMEOUT_10S = 10000
        val TIMEOUT_30S = 30000

        var loginId: Long = 0
    }

}
