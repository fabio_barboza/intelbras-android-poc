package br.com.intelbras.poc.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.support.v7.app.AppCompatActivity
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.View

import android.content.Intent
import android.widget.Toast
import br.com.intelbras.poc.R
import br.com.intelbras.poc.service.SdkService
import com.company.NetSDK.INetSDK
import com.company.NetSDK.NET_DEVICEINFO_Ex

import kotlinx.android.synthetic.main.activity_login.*

/**
 * A login screen that offers login via url/port/username/password.
 */
class LoginActivity : AppCompatActivity() {
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private var loginTask: SdkLoginTask? = null

    //var loginId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        SdkService.instance.init()
        login.setOnClickListener { attemptLogin() }
    }

    override fun onDestroy() {
        INetSDK.Logout(SdkService.loginId)
        SdkService.instance.cleanup()
        super.onDestroy()
    }

    /**
     * Attempts to sign in.
     */
    private fun attemptLogin() {
        if (loginTask != null) {
            return
        }

        // Reset errors.
        url.error = null
        port.error = null
        username.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val url = url.text.toString()
        val port = Integer.parseInt(port.text.toString())
        val username = username.text.toString()
        val password = password.text.toString()

        var cancel = false
        var focusView: View? = null

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            loginTask = SdkLoginTask(url, port, username, password)
            loginTask!!.execute(null as Void?)
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 0 else 1).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_form.visibility = if (show) View.GONE else View.VISIBLE
                    }
                })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 1 else 0).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_progress.visibility = if (show) View.VISIBLE else View.GONE
                    }
                })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    /**
     * Represents an asynchronous login.
     */
    inner class SdkLoginTask internal constructor(
        private val url: String,
        private val port: Int,
        private val username: String,
        private val password: String
    ) :
        AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void): Boolean? {
            val err = 0
            val deviceInfo = NET_DEVICEINFO_Ex()
            SdkService.loginId = INetSDK.LoginEx2(url, port, username, password, 20, null, deviceInfo, err)

            if (0L == SdkService.loginId) {
                return false
            }
            return true
        }

        override fun onPostExecute(success: Boolean?) {
            loginTask = null
            showProgress(false)

            if (success!!) {
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            } else {
                if (0L == SdkService.loginId) {
                    val mErrorCode = INetSDK.GetLastError()
                    Toast.makeText(this@LoginActivity, "Fail to connect to $url\n$mErrorCode", Toast.LENGTH_LONG).show()
                }
            }
        }

        override fun onCancelled() {
            loginTask = null
            showProgress(false)
        }
    }
}
