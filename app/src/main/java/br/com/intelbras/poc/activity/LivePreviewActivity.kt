package br.com.intelbras.poc.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.WindowManager
import android.widget.Toast
import br.com.intelbras.poc.R
import br.com.intelbras.poc.service.SdkService
import com.company.NetSDK.CB_fRealDataCallBackEx
import com.company.NetSDK.INetSDK
import com.company.NetSDK.SDK_RealPlayType
import com.company.PlaySDK.IPlaySDK

class LivePreviewActivity : AppCompatActivity(), SurfaceHolder.Callback {

    private val TAG = LivePreviewActivity::class.java.simpleName
    private var surfaceView: SurfaceView? = null
    private var playPort = 0
    private var handler: Long = 0
    private val STREAM_BUF_SIZE = 1024 * 1024 * 2
    private val RAW_AUDIO_VIDEO_MIX_DATA = 0
    private var mRealDataCallBackEx: CB_fRealDataCallBackEx? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_preview)
        setTitle("Live View")
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        setupView()
        this.startPlay(0, 0, surfaceView!!)
    }

    private fun setupView() {
        //Esse método não funciona com o SDK da Intelbras
        playPort = IPlaySDK.PLAYGetFreePort()
        surfaceView = findViewById(R.id.real_view) as SurfaceView
        surfaceView!!.holder.addCallback(this)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        //Esse método não funciona com o SDK da Intelbras
        IPlaySDK.InitSurface(playPort, surfaceView)
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {

    }

    override fun onDestroy() {
        stopRealPlay()
        surfaceView = null
        super.onDestroy()
    }

    fun stopRealPlay() {
        try {
            IPlaySDK.PLAYStop(playPort)
            IPlaySDK.PLAYStopSoundShare(playPort)
            IPlaySDK.PLAYCloseStream(playPort)
            INetSDK.StopRealPlayEx(handler)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        handler = 0
    }

    fun prePlay(channel: Int, streamType: Int, sv: SurfaceView): Boolean {
        handler = INetSDK.RealPlayEx(SdkService.loginId, channel, streamType)
        if (handler == 0L) {
            return false
        }
        val isOpened = if (IPlaySDK.PLAYOpenStream(playPort, null, 0, STREAM_BUF_SIZE) == 0) false else true
        if (!isOpened) {
            Log.d(TAG, "OpenStream Failed")
            return false
        }
        val isPlayin = if (IPlaySDK.PLAYPlay(playPort, sv) == 0) false else true
        if (!isPlayin) {
            Log.d(TAG, "PLAYPlay Failed")
            IPlaySDK.PLAYCloseStream(playPort)
            return false
        }
        val isSuccess = if (IPlaySDK.PLAYPlaySoundShare(playPort) == 0) false else true
        if (!isSuccess) {
            Log.d(TAG, "SoundShare Failed")
            IPlaySDK.PLAYStop(playPort)
            IPlaySDK.PLAYCloseStream(playPort)
            return false
        }
        return true
    }

    fun startPlay(channel: Int, streamType: Int, view: SurfaceView) {
        Log.d(TAG, "StreamTpye: " + SDK_RealPlayType.SDK_RType_Realplay_0)
        if (!prePlay(channel, SDK_RealPlayType.SDK_RType_Realplay_0, view)) {
            Toast.makeText(this, "Falha ao tentar iniciar o Live View", Toast.LENGTH_LONG).show()
            Log.d(TAG, "prePlay returned false..")
            return
        }
        if (handler != 0L) {
            mRealDataCallBackEx = CB_fRealDataCallBackEx { rHandle, dataType, buffer, bufSize, param ->
                Log.v(TAG, "dataType:$dataType; bufSize:$bufSize; param:$param")
                if (RAW_AUDIO_VIDEO_MIX_DATA == dataType) {
                    Log.i(TAG, "dataType == 0")
                    IPlaySDK.PLAYInputData(playPort, buffer, buffer.size)
                }
            }
            INetSDK.SetRealDataCallBackEx(handler, mRealDataCallBackEx, 1)
        }
    }

}