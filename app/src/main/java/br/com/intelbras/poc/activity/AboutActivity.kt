package br.com.intelbras.poc.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.intelbras.poc.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
