package br.com.intelbras.poc.service

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.company.NetSDK.*
import com.company.PlaySDK.Constants
import com.company.PlaySDK.IPlaySDK
import kotlin.experimental.and


class PlaybackService(internal var mContext: Context?) {
    private val EMPTY_BIT_STREAM_LIBRARY = 1
    private val EMPTY_PLAY_QUEUE = 3
    private val INVALID_OFFSET_RELATIVE_FILES = -0x1
    private val STREAM_BUFFER_SIZE = 1024 * 1024 * 2
    internal var mLoginHandler: Long = 0
    @Volatile
    internal var mPlayHandler: Long = 0
    internal var mCallBack: OnPlayBackCallBack? = null
    internal var mSurface: SurfaceView? = null
    @Volatile
    var port: Int = 0
        internal set

    val osDtime: NET_TIME?
        get() {
            val rr = ByteArray(24)
            var year: Long = 0
            var month: Long = 0
            var day: Long = 0
            var hour: Long = 0
            var minute: Long = 0
            var second: Long = 0
            val gf = 0

            if (IPlaySDK.PLAYQueryInfo(0, Constants.PLAY_CMD_GetTime, rr, rr.size, gf) != 0) {
                year = ((rr[3] and 0xff.toByte()).toLong() shl 24) + ((rr[2] and 0xff.toByte()).toLong() shl 16) +
                        ((rr[1] and 0xff.toByte()).toLong() shl 8) + (rr[0] and 0xff.toByte()).toLong()
                month = ((rr[7] and 0xff.toByte()).toLong() shl 24) + ((rr[6] and 0xff.toByte()).toLong() shl 16) +
                        ((rr[5] and 0xff.toByte()).toLong() shl 8) + (rr[4] and 0xff.toByte()).toLong()
                day = ((rr[11] and 0xff.toByte()).toLong() shl 24) + ((rr[10] and 0xff.toByte()).toLong() shl 16) +
                        ((rr[9] and 0xff.toByte()).toLong() shl 8) + (rr[8] and 0xff.toByte()).toLong()
                hour = ((rr[15] and 0xff.toByte()).toLong() shl 24) +
                        ((rr[14] and 0xff.toByte().toByte()).toLong() shl 16) +
                        ((rr[13] and 0xff.toByte()).toLong() shl 8) + (rr[12] and 0xff.toByte()).toLong()
                minute = ((rr[19] and 0xff.toByte()).toLong() shl 24) + ((rr[18] and 0xff.toByte()).toLong() shl 16) +
                        ((rr[17] and 0xff.toByte()).toLong() shl 8) + (rr[16] and 0xff.toByte()).toLong()
                second = ((rr[23] and 0xff.toByte()).toLong() shl 24) + ((rr[22] and 0xff.toByte()).toLong() shl 16) +
                        ((rr[21] and 0xff.toByte()).toLong() shl 8) + (rr[20] and 0xff.toByte()).toLong()
                val time = NET_TIME()
                time.dwYear = year
                time.dwMonth = month
                time.dwDay = day
                time.dwHour = hour
                time.dwMinute = minute
                time.dwSecond = second
                return time
            }
            return null

        }


    init {
        mLoginHandler = SdkService.loginId

    }

    fun initSurfaceView(streamType: Int, sv: SurfaceView) {
        port = IPlaySDK.PLAYGetFreePort()
        var stream = SDK_RealPlayType.SDK_RType_Multiplay
        if (streamType == 1)
            stream = SDK_RealPlayType.SDK_RType_Realplay_0
        if (!setDeviceMode(stream, NET_RECORD_TYPE.NET_RECORD_TYPE_ALL)) {
            return
        }
        this.mSurface = sv

        sv.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                IPlaySDK.InitSurface(port, sv)
                Log.d(TAG, "surfaceCreated")
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
                Log.d(TAG, "surfaceChanged")
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                Log.d(TAG, "surfaceDestroyed")
            }
        })
        initPlayer(this.mSurface)
    }

    fun setDeviceMode(streamType: Int, recordType: Int): Boolean {
        if (!INetSDK.SetDeviceMode(mLoginHandler, EM_USEDEV_MODE.SDK_RECORD_STREAM_TYPE, streamType))
            return false
        return if (!INetSDK.SetDeviceMode(mLoginHandler, EM_USEDEV_MODE.SDK_RECORD_TYPE, recordType)) false else true
    }

    fun startPlayBack(channelID: Int, startTime: NET_TIME, endTime: NET_TIME): Int {
        if (mLoginHandler == 0L)
            return -1
        logTime("startTime", startTime)
        logTime("endTime", endTime)
        mPlayHandler = INetSDK.PlayBackByTimeEx(mLoginHandler,
            channelID,
            startTime,
            endTime,
            this.mCallBack,
            CB_fDataCallBack { l, i, buffers, i1 -> IPlaySDK.PLAYInputData(port, buffers, buffers.size) })

        if (mPlayHandler == 0L)
            return INetSDK.GetLastError() and 0x7fffffff
        IPlaySDK.PLAYSetPlayedTimeEx(port, 0)
        IPlaySDK.PLAYResetBuffer(port, EMPTY_BIT_STREAM_LIBRARY)
        IPlaySDK.PLAYResetBuffer(port, EMPTY_PLAY_QUEUE)
        return NO_ERROR
    }

    fun logTime(head: String, time: NET_TIME) {
        Log.i(
            TAG, head + " Year:" + time.dwYear + ";Month:" + time.dwMonth + ";Day:" + time.dwDay +
                    ";Hour:" + time.dwHour + ";Minute:" + time.dwMinute + ";Second:" + time.dwSecond
        )
    }


    fun play(play: Boolean): Boolean {
        if (mPlayHandler == 0L)
            return false
        if (!play) {
            if (IPlaySDK.PLAYPause(port, 1.toShort()) == 0)
                return false
        } else {
            if (IPlaySDK.PLAYPause(port, 0.toShort()) == 0)
                return false
        }
        return INetSDK.PausePlayBack(mPlayHandler, play)
    }

    fun setCallBack(callBack: OnPlayBackCallBack) {
        this.mCallBack = callBack
    }

    private fun initPlayer(view: SurfaceView?): Boolean {
        if (view == null)
            throw NullPointerException("the parameter Surface is null")
        if (!(if (IPlaySDK.PLAYOpenStream(port, null, 0, STREAM_BUFFER_SIZE) == 0) false else true))
            return false
        IPlaySDK.PLAYSetStreamOpenMode(port, Constants.STREAME_FILE)
        if (!(if (IPlaySDK.PLAYPlay(port, view) == 0) false else true)) {
            IPlaySDK.PLAYCloseStream(port)
            return false
        }
        val result = IPlaySDK.PLAYPlaySoundShare(port)
        if (result == 0) {
            IPlaySDK.PLAYStop(port)
            IPlaySDK.PLAYCloseStream(port)
            return false
        }
        return true
    }

    fun stopPlayBack() {
        INetSDK.StopPlayBack(mPlayHandler)
        IPlaySDK.PLAYStop(port)
        IPlaySDK.PLAYCloseStream(port)
        IPlaySDK.PLAYStopSoundShare(port)
        port = 0
        mPlayHandler = 0
    }

    fun release() {
        stopPlayBack()
        mCallBack = null
        mLoginHandler = 0
        mContext = null
    }

    interface OnPlayBackCallBack : CB_fDownLoadPosCallBack

    fun doPlayBack(
        stream: Int,
        sv: SurfaceView,
        channel: Int,
        start: NET_TIME,
        end: NET_TIME,
        callback: OnPlayBackTaskDone
    ) {
        val task = PlayBackTask(callback)
        task.execute(stream, sv, channel, start, end)
    }

    private inner class PlayBackTask(private val callback: OnPlayBackTaskDone?) : AsyncTask<Any, Any, Int>() {
        override fun onPreExecute() {
            super.onPreExecute()
            stopPlayBack()
        }

        override fun doInBackground(params: Array<Any>): Int? {
            val stream = params[0] as Int
            val sv = params[1] as SurfaceView
            val channel = params[2] as Int
            val start = params[3] as NET_TIME
            val end = params[4] as NET_TIME
            initSurfaceView(stream, sv)
            return startPlayBack(channel, start, end)
        }

        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)
            val r = if (result == 0) true else false
            if (this.callback != null) {
                this.callback.onTaskDone(r)
            }
        }

    }

    interface OnPlayBackTaskDone {
        fun onTaskDone(result: Boolean)
    }

    companion object {
        private val TAG = PlaybackService::class.java.simpleName
        private val NO_ERROR = 0
    }
}