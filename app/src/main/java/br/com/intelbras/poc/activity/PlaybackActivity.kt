package br.com.intelbras.poc.activity


import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import br.com.intelbras.poc.R
import br.com.intelbras.poc.service.PlaybackService
import com.company.NetSDK.NET_TIME
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class PlaybackActivity : AppCompatActivity(), PlaybackService.OnPlayBackCallBack, View.OnClickListener {
    internal var mPlayBackModule: PlaybackService? = null
    internal var mSurface: SurfaceView? = null
    internal var mSeekbar: SeekBar? = null
    internal var mCurrentOSDTime: TextView? = null
    internal var isPlaying = false
    private val NORMAL_SPEED_INDEX = 2
    internal var CURRENT_SPEED_INDEX = NORMAL_SPEED_INDEX
    internal var mPlayButton: Button? = null
    internal var mStartAndPause: Button? = null
    private val UPDATE_PLAYBACK_PROGRESS = 0x25

    internal var mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            Log.d(TAG, "CurrentProgress:" + mSeekbar!!.getProgress());
            when (msg.what) {
                UPDATE_PLAYBACK_PROGRESS -> {
                    val offset = msg.obj as Long
                    mSeekbar!!.progress = offset.toInt()
                }
                else -> {
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle("Playback")
        setContentView(R.layout.activity_playback)
        setupView()
        mPlayBackModule = PlaybackService(this)
        mPlayBackModule!!.setCallBack(this)
    }

    private fun setupView() {
        mSurface = findViewById(R.id.play_back_view) as SurfaceView
        mPlayButton = findViewById(R.id.start_playback) as Button
        mPlayButton!!.setOnClickListener(this)
        mSeekbar = findViewById(R.id.play_back_seekbar) as SeekBar
        mSeekbar!!.progress = 0
        mSeekbar!!.setOnSeekBarChangeListener(PlayBackProgress())

        mStartAndPause = findViewById(R.id.play_start_pause) as Button
        mStartAndPause!!.setOnClickListener(this)

        mCurrentOSDTime = findViewById(R.id.current_osd_time) as TextView
    }

    private inner class PlayBackProgress : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            Log.i(TAG, "onProgressChanged")
            val time = cal(progress)
            var hour = ""
            var minute = ""
            var second = ""
            if (time.dwHour < 10) {
                hour = "0" + time.dwHour
            } else {
                hour = time.dwHour.toString()
            }
            if (time.dwMinute < 10) {
                minute = "0" + time.dwMinute
            } else {
                minute = time.dwMinute.toString()
            }
            if (time.dwSecond < 10) {
                second = "0" + time.dwSecond
            } else {
                second = time.dwSecond.toString()
            }
            val temp = "$hour:$minute:$second"
            mCurrentOSDTime!!.text = temp
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {
            Log.i(TAG, "onStartTrackingTouch")
        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {
            Log.i(TAG, "onStopTrackingTouch")
            val progress = seekBar.progress
            val start = cal(progress)
            val end = getEndTime(start)
            mPlayBackModule!!.logTime("Start", start)
            mPlayBackModule!!.logTime("End", end)
            startPlayBack(start, end)

        }
    }

    private fun dip2px(dpValue: Float): Int {
        val scale = resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    private fun cal(second: Int): NET_TIME {
        var h = 0
        var d = 0
        var s = 0
        val temp = second % 3600
        if (second > 3600) {
            h = second / 3600
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60
                    if (temp % 60 != 0) {
                        s = temp % 60
                    }
                } else {
                    s = temp
                }
            }
        } else {
            d = second / 60
            if (second % 60 != 0) {
                s = second % 60
            }
        }
        val time = getStartTime(SimpleDateFormat("yyyy-MM-dd").format(Date()))
        time!!.dwHour = h.toLong()
        time.dwMinute = d.toLong()
        time.dwSecond = s.toLong()
        return time
    }

    override operator fun invoke(playHandler: Long, totalSize: Int, downloadSize: Int) {
        val msg: Message
        val time = mPlayBackModule!!.osDtime
        if (time != null) {
            msg = mHandler.obtainMessage(UPDATE_PLAYBACK_PROGRESS)//send message to UI Thread  update seekbar.
            val second = time!!.dwHour * 60 * 60 + time!!.dwMinute * 60 + time!!.dwSecond
            msg.obj = second
            mHandler.sendMessage(msg)
        }
    }

    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.play_start_pause -> {
                isPlaying = !isPlaying
                play(isPlaying, v)
            }
            R.id.start_playback -> {
                val button = v as Button
                if (button.text == getString(R.string.start_play_back)) {
                    val start = getStartTime(SimpleDateFormat("yyyy-MM-dd").format(Date()))
                    if (start == null) {
                        Toast.makeText(this, "Failed to start Playback", Toast.LENGTH_LONG).show()
                    }
                    val end = getEndTime(start!!)
                    startPlayBack(start, end)
                } else if (button.text == getString(R.string.stop_play_back)) {
                    stopPlayBack()
                }
            }
            else -> {
            }
        }
    }

    private fun getEndTime(time: NET_TIME): NET_TIME {
        val t = NET_TIME()
        t.dwYear = time.dwYear
        t.dwMonth = time.dwMonth
        t.dwDay = time.dwDay
        t.dwHour = 23
        t.dwMinute = 59
        t.dwSecond = 59
        return t
    }

    private fun getStartTime(time: String?): NET_TIME? {
        if (time == null || time == "")
            return null
        val pattern = Pattern.compile("[^0-9]")
        val matcher = pattern.matcher(time)
        val temp = matcher.replaceAll(" ").split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (temp.size < 3)
            return null
        val Time = NET_TIME()
        Time.dwYear = Integer.parseInt(temp[0]).toLong()
        Time.dwMonth = Integer.parseInt(temp[1]).toLong()
        Time.dwDay = Integer.parseInt(temp[2]).toLong()
        Time.dwHour = 0
        Time.dwMinute = 0
        Time.dwSecond = 0
        return Time
    }

    private fun clearSurface() {
        if (mSurface != null) {
            mSurface!!.setBackgroundColor(Color.BLACK)
        }
    }

    private fun play(playing: Boolean, v: View) {
        if (!mPlayBackModule!!.play(playing)) {
            isPlaying = !isPlaying
            Toast.makeText(this, "Playback failure", Toast.LENGTH_LONG).show()
            return
        }
        if (!playing) {
            (v as Button).setText(R.string.play)
        } else {
            (v as Button).setText(R.string.pause)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayBackModule!!.release()
        mPlayBackModule = null
    }

    private fun reset() {
        CURRENT_SPEED_INDEX = NORMAL_SPEED_INDEX
        isPlaying = false
        mPlayButton!!.setText(R.string.start_play_back)
        mStartAndPause!!.setText(R.string.play)
    }

    private fun stopPlayBack() {
        mPlayBackModule!!.stopPlayBack()
        reset()
        clearSurface()
        mHandler.post {
            mSeekbar!!.progress = 0
            mCurrentOSDTime!!.text = ""
        }
    }

    private fun startPlayBack(start: NET_TIME, end: NET_TIME) {
        reset()
        mPlayBackModule!!.doPlayBack(0,
            mSurface!!,
            0,
            start,
            end,
            object : PlaybackService.OnPlayBackTaskDone {
                override fun onTaskDone(result: Boolean) {
                    if (!result) {
                        Toast.makeText(this@PlaybackActivity, "Playback failure", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        mSurface!!.setBackgroundColor(Color.TRANSPARENT)
                        isPlaying = true
                        mPlayButton!!.setText(R.string.stop_play_back)
                        mStartAndPause!!.setText(R.string.pause)
                    }
                }
            })
    }

    companion object {
        private val TAG = PlaybackActivity::class.java.simpleName
    }
}
